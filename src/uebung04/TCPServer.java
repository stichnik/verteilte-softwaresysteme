package uebung04;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class TCPServer {
	private static int portnum = 4711;
	
	
	public static void initialize() {
		ServerSocket serversock=null;
		try {
			//Serversock setup
			serversock = new ServerSocket(portnum);
			serversock.setSoTimeout(1000);
			System.out.println("TCP Echo Server started up on port " + serversock.getLocalPort()+ " and waiting for connections.\n Shutdown with Enter.");
			int threadcount=0;
			//While no inputs in console buffer...
			while(System.in.available()==0) {
				try {
					Socket client = serversock.accept();
					//We create a new Thread with our Thread class 
					//Because it merely inherits the runnable interface, we have to create a seperate thread, then we run it
					(new Thread(new TCPEchoThread(threadcount++,client))).start();
					
				}catch (SocketTimeoutException e1) {
					//We didn't get a new connection in 1000ms, so just check again
				}
			}
			while(System.in.available()>0) System.in.read(); //Clean up the console buffer
		} catch (Exception e) {
			System.out.println("Exception in main thread, stacktrace:");
			e.printStackTrace();
		}
		if(serversock!=null) {
			try {
				serversock.close();
				System.out.println("---Server stopped.");
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
	}
}
