package uebung04;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class UDPServer {

	private static int portnum = 4711;
	private static int packetsize = 512;
	public static void	initialize() {
		try {
			DatagramSocket serversock = new DatagramSocket(portnum);
			DatagramPacket packet = new DatagramPacket(new byte[packetsize], packetsize);
			System.out.println("UDP Echo Server started up on port " + serversock.getLocalPort());
			while(true) {
				packet.setLength(packetsize);
				serversock.receive(packet);
				byte[] data = packet.getData();
				String line = new String(data,0,packet.getLength());
				if(packet.getLength()==0||line.equals("---shutdown"))break;
				System.out.println("--- received: "+line);
				packet.setAddress(packet.getAddress());
				packet.setPort(packet.getPort());
				serversock.send(packet);
			}
			serversock.close();
			System.out.println("--- Server stopped");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

