package uebung04;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;


public class TCPEchoThread implements Runnable {
	private int num;
	private Socket client;
	public TCPEchoThread(int threadnum,Socket client) {
		//We save a reference to the Socket
		num=threadnum;
		this.client=client;
	}

	
	public void run() {
		try {
			BufferedReader tcpin = new BufferedReader(new InputStreamReader(client.getInputStream()));
			BufferedWriter tcpout = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
			System.out.println("---Started thread number "+num+ " for client "+client.getInetAddress());
			//We announce ourselves to the client
			tcpout.write("---Welcome to the TCP Server, speaking to thread number "+num);
			tcpout.newLine();
			tcpout.flush();
			String line;
			while((line=tcpin.readLine())!=null) {
				//Print received message to console and send back
				System.out.println("---Thread "+num+" received line:"+line);
				tcpout.write(line);
				tcpout.newLine(); tcpout.flush();
			}
			//Client closed connection
			System.out.println("Thread number "+num+" shutting down...");
			client.close();
		} catch (Exception e) {
			System.out.println("Exception in thread "+num+", Stacktrace:");
			e.printStackTrace();
		}
		
		
	}
	
}
