package uebung04;



public class Main {

	
	public static void main(String[] args) {
		//System.out.println(args[0]);
		if(args.length==0) {
			System.out.print("No args, use -c for client or -s for server");
			return;}
		if(args.length==1) {
			System.out.println("Please specify either -udp or -tcp");
			return;
		}
		
		if(args[0].equals("-s")) {
			System.out.println("Starting server...");
			if(args[1].contentEquals("-udp")) {
				UDPServer.initialize();
			}else if(args[1].contentEquals("-tcp")) {
				TCPServer.initialize();
			}
		}else if (args[0].contentEquals("-c")) {
			System.out.println("Starting client...");
			if(args[1].contentEquals("-udp")) {
				Client.initialize(false);
			}else if(args[1].contentEquals("-tcp")) {
				Client.initialize(true);
			}
			
		}else System.out.print("Invalid args, use -c for client or -s for server"); 
		
		
	}

}
