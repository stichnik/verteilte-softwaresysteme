package uebung04;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Client extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static int portnum = 4711;
	private static int packetsize = 512;
	private JTextArea text;
	private JTextField textfield;
	private JButton button;
	//UDP
	private DatagramSocket udpsock;
	private DatagramPacket packet;
	//TCP
	private Socket tcpsock;
	
	private static boolean tcp;
	
	public static void initialize(boolean tcp) {
		Client.tcp=tcp;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new Client();
			}
		});
		
		
	}


	public Client() {
		super("UDP Sender Client");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(text=new JTextArea(), BorderLayout.CENTER);
		text.setPreferredSize(new Dimension(400, 300));
		JPanel bottom=new JPanel();
		bottom.setLayout(new BorderLayout());
		bottom.add(textfield = new JTextField(), BorderLayout.CENTER);
		bottom.add(button = new JButton("Send"), BorderLayout.EAST);
		add(bottom, BorderLayout.SOUTH);
		pack();
		setVisible(true);
		
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tcp) {
					sendRoutineTCP();
				}else {
					sendRoutineUDP();
				}
			}
		});
		addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	try {
		    		if(tcpsock!=null) tcpsock.close();
		    		if(udpsock!=null) udpsock.close();
		    	}catch(Exception e) {
		    		System.out.println("Exception trying to close tcpsocket, stacktrace:");
		    		e.printStackTrace();
		    	}
		    }
		});
		try {
			//TODO: Implement socket and packet setup
			if(tcp) {
				tcpsock = new Socket();
				tcpsock.connect(new InetSocketAddress("localhost", portnum),1000);
				System.out.println("--- Connected to "+tcpsock.getInetAddress().getHostAddress()+"at port "+tcpsock.getPort()+" with local port"+tcpsock.getLocalPort());
				
				Thread TCPReceive = new Thread(new Runnable() {
					public void run() {
						
						try {
							BufferedReader in=new BufferedReader(new InputStreamReader(tcpsock.getInputStream()));
							while(true) {
								if(in.ready()) { //data from network available
									String line = in.readLine();
									System.out.println("--- Received: "+line);
									text.append("Received response: "+line+"\n");
								}
							}
						}catch(SocketTimeoutException e1) {
							//Nothing happens
						}catch(Exception e2) {
							System.out.println("Exception in TCPReceive thread, Stacktrace:");
							e2.printStackTrace();
						}
					}
				});
				TCPReceive.start();
			}else {
				udpsock = new DatagramSocket();
				packet = new DatagramPacket(new byte[packetsize],packetsize);
				System.out.println("UDP Client started up");
				udpsock.setSoTimeout(1000);
				Thread UDPReceive = new Thread(new Runnable() {
					public void run() {
						
						try {
							DatagramPacket receivepacket = new DatagramPacket(new byte[packetsize],packetsize);
							while(true) {
								udpsock.receive(receivepacket);
								text.append("Received response: "+new String(receivepacket.getData(),0,receivepacket.getLength())+"\n");
							}
						}catch (SocketTimeoutException e1) {
							//Nothing happens, we just wait until we get a packet
						}catch (Exception e2) {
							System.out.println("Exception in UDPReceive thread, Stacktrace:");
							e2.printStackTrace();
						}
					}
				});
				UDPReceive.start();
				
			}
		}catch(Exception e) {
			System.out.println("Problem while creating socket/packet, if you are using TCP please check connection and server status. Stacktrace:");
			e.printStackTrace();
		}
		
		
	}
	
	private void sendRoutineUDP() {
		try {
			packet.setAddress(InetAddress.getLocalHost());
			packet.setPort(portnum);
			packet.setData(textfield.getText().getBytes());
			udpsock.send(packet);
			textfield.setText("");
			
		}catch (SocketTimeoutException e1) {
			System.out.println("Expected to get answer from server, but timed out. "
					+ "Check connection and server status. Stacktrace:");
			e1.printStackTrace();
		} catch (Exception e2) {
			System.out.println("Problem trying to send/receive packet, stacktrace:");
			e2.printStackTrace();
		}
		
	}

	private void sendRoutineTCP() {
		try {
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(tcpsock.getOutputStream()));
			String line = textfield.getText();
			System.out.println("--- Sending: "+line);
			out.write(line);
			out.newLine();
			out.flush();
		}catch(Exception e) {
			System.out.println("Exception in TCP send routine, stacktrace:");
			e.printStackTrace();
		}
	}
}
